<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	if (function_exists($_GET['method'])) {
		$_GET['method']();
	}

	// function category
	function getCategory() {
		include 'db.php';

		$sql_category = "SELECT * FROM category";

		// Arrays van all tables
		$query_category	= $conn->query($sql_category);
		$category_arr 	= array();

		//rows -> category array()
		while ($category = $query_category->fetch_assoc()) {
			$category_arr[]	= $category;
		}

		//JSON encode(category) -> Output
		$category_arr = json_encode($category_arr);
		print_r($category_arr);	
	}
?>