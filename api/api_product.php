<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	if (function_exists($_GET['method'])) {
		$_GET['method']($_GET['category_id']);
	}

	function getProduct($id) {
		include 'db.php';

		$sql_product 		= "SELECT * FROM product WHERE category_id=$id";

		// Arrays van all tables
		$query_product	= $conn->query($sql_product);
		$product_arr 	= array();

		//rows -> category array()
		while ($product = $query_product->fetch_assoc()) {
			$product_arr[]	= $product;
		}

		//JSON encode(category) -> Output
		$product_arr = json_encode($product_arr);
		print_r($product_arr);	
	}
?>