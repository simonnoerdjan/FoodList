// Request API
function fetchCategory(){
    $.get("http://app.sr/code_ops/api/api_category.php?method=getCategory", dislayCategory).fail(displayFailure);
}

// Get Results
function dislayCategory(response) {
    var category = $("#page-content");
      $.each(response, function(key, value){
        category.append("<button class='mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored'>" + value.category_name + "</button>");
    });
}

// Fail message
function displayFailure(){
    $('#notification').html("De API is niet beschikbaar");
}